'use strict';

const { consolidatePairs } = require('./library');

test('Correctly consolidating pairs from accounts', () => {
  const accounts = require('./accounts.json');
  accounts.push({ ...accounts[0] });
  accounts.push({ ...accounts[0] });
  accounts.push({ ...accounts[0] });
  accounts[2].pair = 'aaa';

  const result = consolidatePairs(accounts);

  expect(result).toBeDefined();
  expect(result.length).toEqual(2);
  expect(result[0].accounts).toBeDefined();
  expect(result[0].accounts.length).toEqual(3);
  expect(result[1].accounts.length).toEqual(1);
});