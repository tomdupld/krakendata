'use strict';

const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const DDB = require('aws-sdk/clients/dynamodb');

/**
 * Retrieves an object from S3
 * @param Bucket
 * @param Key
 * @returns {Promise<any>}
 */
async function getFromS3(Bucket, Key) {
  const result = await S3.getObject({Bucket, Key}).promise();
  return JSON.parse(result.Body);
}

/**
 * Executes a query and returns the results
 *
 * @param params
 * @returns {Promise<PromiseResult<DocumentClient.QueryOutput, AWSError>>}
 */
async function scan(ddb, params) {
  return ddb.scan(params).promise();
}

/**
 * Accepts all the accounts and returns a list of unique pair/span/seconds combinations
 * @param accounts
 * @returns {{spanCount: *, spanSeconds: *, pair: *}[]}
 */
function consolidatePairs(accounts) {
  const consolidated = new Set();
  accounts.map(account => {
      consolidated.add(account.pair + '-' + account.spanCount + '-' + account.spanSeconds);
    }
  )
  const arr = Array.from(consolidated);
  const uniqueObjects = arr.map(item => {
    const values = item.split('-');
    return {
      pair: values[0],
      spanCount: parseInt(values[1]),
      spanSeconds: parseInt(values[2])
    }
  });

  uniqueObjects.map(obj => {
    obj.accounts = [];
    const found = [];
    accounts.map(acc => {
      if (acc.pair === obj.pair &&
        acc.spanSeconds === obj.spanSeconds &&
        acc.spanCount === obj.spanCount) {
        obj.accounts.push(acc);
      }
    })
  });

  return uniqueObjects;
}

module.exports = {consolidatePairs, getFromS3, scan}