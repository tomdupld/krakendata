'use strict';

const https = require('https');
const DynamoDb = require('aws-sdk/clients/dynamodb');
const AWS = require("aws-sdk");
const moment = require('moment');
const fetch = require('node-fetch');
const ddb = new DynamoDb.DocumentClient({
  apiVersion: '2012-08-10',
  region: 'us-east-1'
});
const {consolidatePairs, scan} = require('./library');
let accounts;
(async function () {
  console.log('Starting up!');
  accounts = await getAllAccounts();
  const consolidatedPairs = consolidatePairs(accounts);

  await Promise.all(consolidatedPairs.map(cons => runForConsolidatedPair(cons)))
    .catch(e => console.log('Problem with running lambdas: ' + e.message));
}());

async function runForConsolidatedPair(consolidatedPair) {
  const interval = 1000 * consolidatedPair.spanSeconds
  const randomWait = Math.random() * 10000;

  setTimeout(async () => await getApiData(consolidatedPair), randomWait);
  setTimeout(async () => setInterval(async () => {
    await getApiData(consolidatedPair)
  }, interval), randomWait);
}

async function getApiData(consolidatedPair) {
  try {
    const ticker = await retry(getTicker(consolidatedPair));
    const pair = Object.keys(ticker.result)[0];
    const newPriceObj = await writeToDynamoDb(pair, ticker.result[pair], consolidatedPair);
    const enriched = await compute(newPriceObj, consolidatedPair);
    await Promise.all(consolidatedPair.accounts.map(account => trade(account, enriched)));
  } catch (e) {
    console.log('Error: ' + e.message);
  }
}

/**
 * Executes the callback, and will retry a few times in case it fails
 * @param callback
 * @returns {Promise<*>}
 */
async function retry(callback) {
  let count = 0;
  const retryCount = 3;
  const retryWaitLength = 1000;
  let response = {success: false};
  while (count < retryCount && !response.success) {
    const promise = new Promise(function (resolve, reject) {
      setTimeout(async function () {
        resolve(await callback);
        count++;
      }, retryWaitLength * count);
    });
    response = await promise;
  }
  if (response.success) {
    return response.data;
  } else {
    throw new Error(response.data)
  }
}

/**
 * Gets the ticker data from Kraken for the given asset pair
 * @param consolidatedPair
 * @returns {Promise<{data: string, success: boolean}>}
 */
async function getTicker(consolidatedPair) {
  let response = {success: true, data: ''};
  try {
    const url = "https://api.kraken.com/0/public/Ticker?pair=" + consolidatedPair.pair;
    const data = await fetch(url);
    response.data = await data.json();
  } catch (e) {
    response.success = false;
    response.data = 'Couldn\'t fetch ticker: ' + e.message;
  }
  return response;
}

/**
 * Writes the Kraken price data to the DynamoDb table
 * @param asset
 * @param o
 * @param consolidatedPair
 * @returns {Promise<{date: string, hi: number, vol: number, lo: number, scheme: string, unix_time: number, ask: number, asset: *, bid: number, open: number}>}
 */
async function writeToDynamoDb(asset, o, consolidatedPair) {
  const dt = +Date.now();
  const scheme = consolidatedPair.spanCount + '-' + consolidatedPair.spanSeconds;
  // vol, hi and lo is for the last 24 hrs
  let newObj = {
    TableName: 'KrakenPriceData',
    Item: {
      'asset': asset,
      'unix_time': dt,
      'ask': parseFloat(o.a[0]),
      'bid': parseFloat(o.b[0]),
      'vol': parseInt(o.v[1]),
      'lo': parseFloat(o.l[1]),
      'hi': parseFloat(o.h[1]),
      'open': parseFloat(o.o),
      'date': moment(dt).format("dddd, MMMM Do YYYY, h:mm:ss a"),
      'scheme': scheme
    },
  };

  try {
    await ddb.put(newObj).promise();
  } catch (e) {
    throw new Error('Couldn\'t write to DynamoDb - ' + e.message)
  }
  return newObj.Item;
}

/**
 * Calls the Compute lambda function
 * @param priceObj
 * @param consolidatedPair
 * @returns {Promise<Lambda.InvocationResponse & {$response: Response<Lambda.InvocationResponse, AWSError>}>}
 */
async function compute(priceObj, consolidatedPair) {
  const lambda = new AWS.Lambda({apiVersion: '2015-03-31', region: 'us-east-1'});
  const params = {
    FunctionName: "Compute-prod-compute",
    InvocationType: "RequestResponse",
    Payload: JSON.stringify(consolidatedPair)
  };

  let result;
  try {
    result = await lambda.invoke(params).promise();
  } catch (e) {
    throw new Error('Couldn\'t invoke Compute lambda - ' + e.message)
  }
  return result;
}

/**
 * Calls the Trade lambda function
 * @param account
 * @param enriched
 * @returns {Promise<Lambda.InvocationResponse & {$response: Response<Lambda.InvocationResponse, AWSError>}>}
 */
async function trade(account, enriched) {
  const lambda = new AWS.Lambda({apiVersion: '2015-03-31', region: 'us-east-1'});
  const params = {
    FunctionName: "Trade-prod-compute",
    InvocationType: "RequestResponse",
    Payload: JSON.stringify({account, priceObj: JSON.parse(JSON.parse(enriched.Payload).body).priceObj.Attributes})
  };

  let result;
  try {
    result = await lambda.invoke(params).promise();
  } catch (e) {
    throw new Error('Couldn\'t invoke Trade lambda - ' + e.message)
  }
  return result;
}

async function getAllAccounts() {
  const params = {
    TableName: 'Accounts',
  };

  const result = await scan(ddb, params);
  return result.Items;
}




